<?php	
	session_start();
	
	$loginOrAccount = 'Log in';
	$loggedinBar = 'empty.html';
	
	// If I am correct the value assigned to isLegit only exists after
	// a succesful login.
	if (isset($_SESSION['isLegit']))
	{
		$loginOrAccount = 'My Account';
		$loggedinBar = 'loggedinBar.html';
	}	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tomorrowland</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="experience.css">
	
</head>
<body id="experienceBody">
	<nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
		<div class="container">
			<div class="navbar-header">

				<!-- logo -->
				<div class="site-branding">
					<a class="logo" href="./">
						<!-- logo image  -->
						<img src="assets/images/logo.png" alt="Logo"> 
					</a>
				</div>

				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

			</div><!-- /.navbar-header -->

			<div class="collapse navbar-collapse" id="navbar-items">
				<ul class="nav navbar-nav navbar-right">
					<li> <a href ="login.php" ><?php echo $loginOrAccount; ?></a></li>
					<?php include($loggedinBar); ?>
				</ul>
			</div>
		</div><!-- /.container -->
	</nav>
	<section id="events">
		<ul id="pastEvents">
		<?php
			// Let us obtain all events that have been organized in the past and put them in an un ordered list as list items.
			include('databasefunctions.php');
			$events = ObtainEventsForCommenting();
			
			// print_r($events);
			
			if ($events != null)
			{
				foreach ($events as $event)
				{
					echo '<a class="clickableRow" href=event.php?eventid='. $event['idEvent'] .'>';
						
						echo '<div class="row">';
							
							echo '<div class="col">';
								echo '<img src='. $event['image'] .'>';
							echo '</div>';
							
							echo '<div class="col">';
								echo  $event['Name'];
							echo '</div>';
							
						echo '</div>';
						
					echo '</a>';
				}
			}
		?>
		</ul>
	</section>
</body>
</html>