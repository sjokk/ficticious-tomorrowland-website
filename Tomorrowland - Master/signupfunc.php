<?php
	include('databasefunctions.php');

	// Very first check if the e-mail already exists in the db.
	$email = $_POST['email'];
	$userInformation = ObtainUserInformation($email);
	
	if (!isset($userInformation) || !isset($userInformation['Dob'])) // Case no info in db yet.
	{
		if ($_POST['password'] == $_POST['confirmpassword']) // Check if pwds match, else redirect.
		{
			// Encrypt password and insert info in db.
			$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
			createuser($email, $password);
			
			// User is new, insert customer data.
			insertCustomerData($_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['telephonenumber'], $_POST['address'], $_POST['number'], $_POST['postcode'], $_POST['city'], $_POST['country'], $_POST['dob']);
			
			sendActivationEmail($email);
				
			echo "Sign up successfull<br>
				 Please visit your e-mail for activation details.";
				
			// Redirect the user after 3 seconds to the log in page.
			header("refresh:3; url=login.php");
		}
		else
		{
			// No matching pwds, rip.
			echo "Your passwords did not match, returning shortly.";
			header("refresh:3; url=Signup.php");
		}
	}
	// If we have a return value, the user is already entered into the db.
	else
	{
		// If the email is confirmed, we remind the user that s/he can log in.
		if ($userInformation['isConfirmed']) // Case info in db and isConfirmed = 1
		{
			echo "$email already exists.<br>";
			echo "You will be redirected to the login page.";
			
			//Redirect to log in, also holds the functionality for forgot password.
			header("refresh:3; url=login.php");
		}
		else // Case info in db and isConfirmed = 0
		{
			// If the email is not confirmed, we send a activation e-mail and update the customer
			// information to what s/he inserted with this sign up (meaning deleting old data in
			// the customer relation where e-mail = e-mail.
			
			// Delete old customer data.
			deleteOldCustomerData($email);
			
			// Insert new data.
			insertCustomerData($_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['telephonenumber'], $_POST['address'], $_POST['number'], $_POST['postcode'], $_POST['city'], $_POST['country'], $_POST['dob']);
			
			// Send activation mail and redirect.
			sendActivationEmail($email);
			echo "$email already exists but is not yet activated.<br>";
			echo "A new activation e-mail has been send to your e-mail address.";
			
			// Redirect to log in.
			header("refresh:3; url=login.php");
		}
	}
	
	function sendActivationEmail($email)
	{	
		$customerInformation = ObtainCustomerInformation($email);
		
		$fName = $customerInformation['First_Name'];
		
		$userInformation = ObtainUserInformation($email);
		$subject = "E-mail verification.";
		
		// Change the url in the e-mail to the url of the host of the website.
		$message = "
		
		Dear $fName
		
		Thanks for registering a TomorrowLand account!
		
		You're just one step away from logging in, that 
		is by clicking the the link to activate your account:
		
		https://i231896.hera.fhict.nl/Tomorrowland/emailVerification.php?email=". $email ."&hash=". $userInformation['activationHash'] ."
		
		With kind regards,
		
		The TomorrowLand Team!";
		
		
		
		$headers = "From:noreply@TomorrowLand.com" . "\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($email, $subject, $message, $headers);
	}
?>