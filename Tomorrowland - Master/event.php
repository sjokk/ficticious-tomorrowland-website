<?php	
	session_start();
	include('databasefunctions.php');
	
	$loginOrAccount = 'Log in';
	$loggedinBar = 'empty.html';
	
	// If I am correct the value assigned to isLegit only exists after
	// a succesful login.
	if (isset($_SESSION['isLegit']))
	{
		$loginOrAccount = 'My Account';
		$loggedinBar = 'loggedinBar.html';
	}
	
	// Safety check to allow a user to only access an eventid via the query string that has passed and was open for commenting.
	if (isset($_GET))
	{
		if (isset($_GET['eventid']))
		{
			$eventId = $_GET['eventid'];
			$result = ObtainEventsForComments($eventId);
			
			if ($result == null)
			{
				header('Location: experience.php');
			}
		}
	}
	
	if ($_POST)
	{		
		$ticketId = ObtainTicketForEvent($_SESSION['customerInfo']['idCustomer'], $eventId)['idTicket'];
		$content = $_POST['comment'];

		if ($content != null && strpos($content, "Enter comment ...") === false)
		{
			UpdateComments($content, $ticketId);
			
			$url = 'event.php?eventid=';
			$url .= $_GET['eventid'];
			
			header('Location: '. $url);
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tomorrowland</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="comment.css">
	
</head>
<body id="commentBody">
	<nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
		<div class="container">
			<div class="navbar-header">

				<!-- logo -->
				<div class="site-branding">
					<a class="logo" href="./">
						<!-- logo image  -->
						<img src="assets/images/logo.png" alt="Logo"> 
					</a>
				</div>

				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

			</div><!-- /.navbar-header -->

			<div class="collapse navbar-collapse" id="navbar-items">
				<ul class="nav navbar-nav navbar-right">
					<li> <a href ="experience.php" >Comments</a></li>
					<li> <a style="color:black;" href ="login.php" ><?php echo $loginOrAccount; ?></a></li>
					<?php include($loggedinBar); ?>
				</ul>
			</div>
		</div><!-- /.container -->
	</nav>
	<section id="comments">
		<?php
			// Obtain all comments for the first page lets limit this to 25.
			$eventId = $_GET['eventid'];
			$pageNumber = 0;
			
			// Only if the key page has been set with a value.
			if (isset($_GET['page']))
			{
				$pageNumber = (int)$_GET['page'];
			}

			// Query the db for event id and with the appropriate offset based on the pagenumber.
			if (is_int($pageNumber))
			{
				$comments = ObtainComments($eventId, $pageNumber * 25);
			}
			else
			{
				$url = 'event.php?eventId=';
				$url .= $eventId;
				header('Location: '. $url);
			}
			
			$numberOfComments = ObtainNumberOfComments($eventId)[0];
			
			// The current $pageNumber gives us an indication ($pageNumber * 25) of the number of comments left for the next page.
			// In case the current page = 3, 3 * 25 = 75 comments, the count has to exceed this number by a number > 25 to allow for a next page.
			
			$backButton = '';
			if ($pageNumber > 0)
			{
				$previousPage = $pageNumber - 1;
				$backButton = '<a id="previousPage" class="btn btn-primary btn-xs" href="event.php?eventid='. $eventId .'&page='. $previousPage .'"> Previous Page </a>';
			}
			
			$nextButton = '';
			if (($numberOfComments - ($pageNumber * 25)) > 25)
			{
				$nextPage = $pageNumber + 1;
				$nextButton = '<a id="nextPage" class="btn btn-primary btn-xs" href="event.php?eventid='. $eventId .'&page='. $nextPage .'"> Next Page </a>';
			}
			
			foreach ($comments as $comment)
			{
				// Display comment on page.
				
				// Display a row with user information.
				echo '<div class="row mainRow">';
				
					echo '<div class="row posterRow">';
					
						echo '<div class="col">';
							echo $comment['Name'] . ' posted at: '. $comment['date'] .' - '. $comment['time'];
						echo '</div>';
						
						echo '<div class="col">';
							echo '#'. $comment['idComment'];
						echo '</div>';
						
					echo '</div>';
					
					// Another row, with 2 columns, a left column for a profile picture and the right for the content.
					echo '<div class="row contentRow">';
					
						echo '<div class="col left">'; // Must float left.
							echo '<img id="profilePic" src="assets/images/profile_picture.png">';
						echo '</div>';
						
						echo '<div class="col right">'; // Must float left. (is the right column of the row)
							echo $comment['content'];
						echo '</div>';
						
					echo '</div>';
					
				echo '</div>';
			}
			
			// If page number > 0 and $comments is not an empty array, provide users with buttons for next page or back.
			echo '<div id="buttons">';
			
				echo $backButton;
				
				echo $nextButton;
			
			echo '</div>';
		
			
			// print_r($_SESSION);
			
			
			// Below the buttons to scroll, the user will se a form to enter
			// a new comment in case the person holds a ticket for the event
			// that s/he's looking at.
			if (isset($_SESSION['isLegit']))
			{
				// Obtain persons ticket for event s/he's looking at.
				$result = ObtainTicketForEvent($_SESSION['customerInfo']['idCustomer'], $eventId);
				
				if ($result != null)
				{
					// Our target has a ticket for the event.
					// Thus the person gets to view a form to enter a comment. woop woop.
					echo '<div id="formBox">';
						echo '<form method="post" id="commentForm">';
							echo '<textarea id="commentContent" rows="6" name="comment" form="commentForm">Enter comment ...</textarea>';	
						echo '</form>';
						
						echo '<button id="commentButton" class="btn" type="submit" form="commentForm" value="Submit">Post</button>';
						
					echo '</div>';
				}
			}
		?>
	</section>
</body>
</html>