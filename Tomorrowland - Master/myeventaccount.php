<?php
	session_start();
	include('databasefunctions.php');

	$ticketId = $_GET['ticketId'];
	$compEmail = ObtainCustomerEmailForTicketId($ticketId);
	
	if (($compEmail['Email'] != null) && ($compEmail['Email'] == $_SESSION['customerInfo']['Email']))
	{	
		// POTENTIALLY HAVE TO INSERT A CHECK (ISVALID) TICKET ATTRIBUTE TO SEE IF
		// THE EVENT MANAGEMENT STILL ALLOWS THIS EVENT ACCOUNT TO BE USED.
		if ((isset($_POST['amount'])) && ($_POST['amount'] != null))
		{		
			// Update balance for this ticket id.
			updateBalance($ticketId, $_POST['amount']);
			header('Location: ./myeventaccount.php?ticketId=' . $ticketId); // Redirect to same page to avoid F5 re-entering the same amount over and over.
		}
		// Obtain balance for this ticketId.
		$allTicketInfo = ObtainTicketInformation($_SESSION['customerInfo']['idCustomer'], $ticketId);
		
		include('myeventaccount.html');
	}
	else
	{
		header('Location: index.html');
	}
?>