<?php
include('databasefunctions.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Account Overview</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/eventaccount.css">
	<link rel="stylesheet" href="accountOverviewStyle.css">
	
</head>
<body>
<div class="wrapper">
    <div class="panel">
        <div class="panel-header">
		 <h3 class="title">
				<?php echo 'Account overview of Customer id: '. $_SESSION['customerInfo']['idCustomer']; ?>
			</h3>
			</div
			<div class="panel-body"
			
			<div id="ticketQr">
	<?php
		echo "<img src='https://barcode.tec-it.com/barcode.ashx?data=". $_GET['value'] ."&code=QRCode&dpi=96&dataseparator=' alt='Barcode Generator TEC-IT'/>";
	?>
</div>

					
				
			</div>
        </div>
    </div>
	</body>