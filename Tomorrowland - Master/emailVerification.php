<?php
	// include our database functions file.
	include('databasefunctions.php');
	
	// Check if both variables exist (not null) (in the super global get variable).
	if (isset($_GET['email']) && isset($_GET['hash']))
	{
		// If so, assign them and check if they are not empty strings.
		$email = $_GET['email'];
		$hash = $_GET['hash'];
		
		// Check if these values aren't empty strings.
		if (!empty($email) && !empty($hash))
		{
			// Obtain the email and corresponding activation hash from the db.
			$result = ObtainUserInformation($email);
			
			// Test if the obtained values are equal to the values in the query string.
			if (!empty($result))
			{
				// If account is not already activated. 
				if (!$result['isConfirmed'])
				{
					if (($result['email'] == $email) && ($result['activationHash'] == $hash))
					{
						// set the value for isConfimed for this email address to true (1).
						activateAccount($email);
						echo "Your account is now activated.<br>
							  Redirecting you shortly ...";
						
						// Redirect user to log in page.
						header("refresh:3; url=login.php");
					}
					else
					{
						echo "Invalid information.";
					}
				}
				// Otherwise, notify user that the account is already active.
				// Maybe we should remove the hash after the account is activated.
				else
				{
					echo "This account is already activated.<br>
						  Redirecting you shortly ...";
					
					// Redirect user to log in page.
					header("refresh:3; url=login.php");
				}
			}
			// Remove before going live.
			//print_r($result);
		}
	}
	else
	{
		echo "Invalid information.";
	}
?>