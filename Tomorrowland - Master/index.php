<?php 
	session_start();
	
	$loginOrAccount = 'Log in';
	$loggedinBar = 'empty.html';
	
	// If I am correct the value assigned to isLegit only exists after
	// a succesful login.
	if (isset($_SESSION['isLegit']))
	{
		$loginOrAccount = 'My Account';
		$loggedinBar = 'loggedinBar.html';
	}	
	
	require('_index.html');
?>