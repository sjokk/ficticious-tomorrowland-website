<?php
	function ConnectToDb()
	{
		//Create a connection to the database.
		$dsn ='mysql:dbname=dbi231896;host=studmysql01.fhict.local';
		$user ='dbi231896';
		$password = 'UnsafePassword';

		return new PDO($dsn, $user, $password);
	}
	
	function ObtainUserInformation($email)
	{
		$result = querydatabase('select * from loginaccount where email=:email;', [':email'=>$email]);
		
		if ($result != null)
		{
			return $result[0];
		}
	}

	function ObtainNumberOfComments($eventId)
	{
		$sql = "SELECT COUNT(*)
				FROM comment_readable_view
				WHERE idEvent = :eventId;";
		
		$arr = [':eventId'=>$eventId];
		
		$result = querydatabase($sql, $arr);
		
		if ($result != null)
		{
			return $result[0];
		}
	}
	
	function PurchaseTicket($customerId, $eventId)
	{
		$sql = "INSERT INTO ticket (idCustomer, idEvent, isValid) ";
		$sql .= "VALUES (:customerId, :eventId, 1);";
		
		$arr = [':customerId'=>$customerId, ':eventId'=>$eventId];
		
		$result = querydatabaselastid($sql, $arr);
		
		return $result;
	}
	
	function CreateCustomerIfNotExists($firstName, $lastName, $email)
	{
		$sql = "SELECT idCustomer FROM customer WHERE Email = :email;";
		$arr = [':email'=>$email];
		
		$result = querydatabase($sql, $arr);
		if ($result != null)
		{
			$result = $result[0];
		}

		if ($result == null)
		{
			$sql = "INSERT INTO customer (First_Name, Last_Name, Email)
				VALUES (:fName, :lName, :email);";
			$arr = [':fName'=>$firstName, ':lName'=>$lastName, ':email'=>$email];

			$result = querydatabaselastid($sql, $arr);
		}
		return $result;
	}
	
	function ObtainCustomerInformation($email)
	{
		$result = querydatabase('SELECT * FROM customer WHERE Email = :email', [':email'=>$email]);
		
		if ($result != null)
		{
			return $result[0];
		}
	}
	
	// Obtains the customer's email address for inserted ticket id.
	
	// Is used to validate that if a person goes to eventaccount.php page,
	// the person is also the one logged in on the email address that be-
	// longs to that ticketId. (safety)
	function ObtainCustomerEmailForTicketId($ticketId)
	{
		$sql = 'SELECT c.Email FROM customer c JOIN ticket t ON (c.idCustomer = t.idCustomer) JOIN event e ON (e.idEvent = t.idEvent) WHERE e.IsActivated = 1 AND t.idTicket = :ticketId';
		$arr = [':ticketId'=>$ticketId];
		
		$result = querydatabase($sql, $arr);
		
		if ($result != null)
		{
			return $result[0];
		}
	}
	
	function ObtainTicketRow($ticketId)
	{
        $result = querydatabase('SELECT * FROM ticket WHERE idTicket = :idTicket', [':idTicket'=>$ticketId]);

        if($result != null)
        {
            return $result[0];
        }
	}

	// Obtains all ticket information for input (customer id, ticket id) combination.
	function ObtainTicketInformation($idCustomer, $ticketId)
    {
        $result = querydatabase('SELECT * FROM ticket WHERE idCustomer = :idCustomer AND idTicket = :idTicket', [':idCustomer'=>$idCustomer, ':idTicket'=>$ticketId]);

        if($result != null)
        {
            return $result[0];
        }
    }
	
	
	function UpdateComments($content, $ticketId)
	{
		$sql = "INSERT INTO comment (Content, idTicket) VALUES (:content, :idTicket);";
		$arr = [':content'=>$content, ':idTicket'=>$ticketId];
		
		querydatabase($sql, $arr);
	}
	
	// Obtains all ticket information for input (customer id, ticket id) combination.
	function ObtainTicketForEvent($idCustomer, $eventId)
    {
        $result = querydatabase('SELECT * FROM ticket WHERE idCustomer = :idCustomer AND idEvent = :eventId', [':idCustomer'=>$idCustomer, ':eventId'=>$eventId]);

        if($result != null)
        {
            return $result[0];
        }
    }
	
	function querydatabaselastid($sql, $associativeArray)
	{
		$result = null;
		try
		{
			$conn = ConnectToDb();
			$sth = $conn->prepare($sql);
			$sth->execute($associativeArray);
			$id = $conn->query("select last_insert_id() as last_id;");
			$result = $id->fetch();

			$conn = null;
		}

		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		return $result;
	}
	
	// Query's the database based on variables by. 
	// 
	// $sql is a query string and $associativeArray, an associative array
	// of key, value pairs that will be input into the query string.
	function querydatabase($sql, $associativeArray)
	{
		$result = null;
		try
		{
			$conn = ConnectToDb();
			$sth = $conn->prepare($sql);
			$sth->execute($associativeArray);
			$result = $sth->fetchAll();

			$conn = null;
		}

		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		return $result;
	}
	
	// Function that is similar to querydatabase except it does not make use of prepare
	// and only of the query method from the class since no user input values are used.
	function executeQuery($sql)
	{
		$result = null;
		try
		{
			$conn = ConnectToDb();
			$row = $conn->query($sql);
			$result = $row->fetch();
			$conn = null;
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $result;
	}

	// Obtains the email address from the loginaccount table based on the inserted email address. (thus only returns an email if there exists an email in the table.)
	function CheckLogIn($email)
	{
		$result = querydatabase('select email from loginaccount where email=:email;', [':email'=>$email]);
		if($result != null)
		{
			$result = $result[0];
		}
		return $result;
	}

	// Inserts an email password combination with an activationHash (md5 hash based on random number seed) into the database.
	function createuser($email, $password)
	{

		$activationHash = md5(rand(0, 1000));
		querydatabase('insert into loginaccount(`email`, `password`, `activationHash`) values (:email, :password, :activationHash);', [':email'=>$email, ':password'=>$password, ':activationHash'=>$activationHash]);
	}
	
	// Function to update the isConfirmed value to 1 (true) that the user's account is now verified.
	function activateAccount($email)
	{
		querydatabase('UPDATE loginaccount SET isConfirmed = 1 WHERE email=:email', [':email'=>$email]);
	}
	
	// Function to obtain all the current event information.
	function ObtainCurrentEvent()
	{
		$sql = 'SELECT * FROM event WHERE SYSDATE() <= End_Date AND Start_Date = ( SELECT MIN(Start_Date) FROM event WHERE Start_Date >= SYSDATE() );';
		$array = executeQuery($sql);
		
		if ($array != null)
		{
			return $array;
		}
	}
	
	// Returns the value of Balance for input ticket id.
	function ObtainBalance($ticketId)
	{
		$sql = "SELECT Balance FROM ticket WHERE idTicket = :ticketId;";
		$arr = [':ticketId' => $ticketId];
		
		$result = querydatabase($sql, $arr);
		
		if ($result != null)
		{
			return $result[0];
		}
		
	}
	
	function ObtainEventsForCommenting()
	{
		$sql = "SELECT * FROM events_for_commenting_view;";
		
		return querydatabase($sql, []);
	}
	
	function ObtainEventsForComments($eventId)
	{
		$sql = "SELECT * FROM events_for_commenting_view WHERE idEvent = :eventId;";
		
		$result = querydatabase($sql, [':eventId'=>$eventId]);
		
		if ($result != null)
		{
			return $result[0];
		}
	}
	
	function ObtainEvent($eventId)
	{
		$sql = "SELECT * FROM customer_ticket_owned_view WHERE idEvent = :eventId;";
		$arr = [':eventId' => $eventId];
		
		$result = querydatabase($sql, $arr);
		
		if($result != null)
		{
			return $result[0];
		}
	}
	
	// Updates Balance by amount for ticket id and inserts amount into the transaction history table.
	function updateBalance($ticketId, $amount)
	{
		$oldBalance = ObtainBalance($ticketId)['Balance'];
		$newBalance = $oldBalance + $amount;
		
		$sql = 'UPDATE ticket SET Balance = :newBalance WHERE idTicket = :ticketId;';
		$arr = [':ticketId'=>$ticketId, ':newBalance'=>$newBalance];
		
		querydatabase($sql, $arr);
		
		$sql = 'INSERT INTO eventaccount_deposit (Amount, idTicket) VALUES (:amount, :ticketId);';
		$arr = [':ticketId'=>$ticketId, ':amount'=>$amount];
		querydatabase($sql, $arr);
	}
	
	function ObtainTicketTransactionHistory($ticketId)
	{
		$sql = 'SELECT * FROM customer_transaction_view WHERE idTicket = :ticketId ORDER BY DateTime DESC LIMIT 10;';
		$arr = [':ticketId'=>$ticketId];
		
		$result = querydatabase($sql, $arr);
		
		if ($result != null)
		{
			return $result;
		}
	}
	
	function ObtainAvailableCampingSpots($eventId)
	{
		$sql = "SELECT t.idEvent, c.idCampingspot, c.Name, (6 - COUNT(*)) AS available_spots
				FROM campingspot c
				JOIN ticket t
				ON (c.idCampingspot = t.idCampingspot)
				WHERE t.idEvent = :idEvent
				GROUP BY c.Name
                HAVING available_spots > 0
				UNION
				SELECT NULL, c.idCampingspot, c.Name, 6
				FROM campingspot c
				WHERE c.idCampingspot NOT IN (
					SELECT t.idCampingspot
					from ticket t
					WHERE t.idCampingspot IS NOT NULL
					AND t.idEvent = :idEvent
				);";
		
		$arr = [':idEvent'=>$eventId];
		
		$result = querydatabase($sql, $arr);
		
		return $result;
	}
	
	function ObtainTicketTransactionHistoryNext($ticketId, $offset)
	{		
		$sql = "SELECT * FROM customer_transaction_view WHERE idTicket = :ticketId ORDER BY DateTime DESC LIMIT 10 OFFSET $offset;";
		$arr = [':ticketId'=>$ticketId];
		
		$result = querydatabase($sql, $arr);
		
		
		return $result;
	}
	
	function ObtainEventReadable($eventId)
	{
		$sql = "SELECT * FROM event_information_view WHERE idEvent = :eventId;";
		$arr = [':eventId'=>$eventId];
		
		$result = querydatabase($sql, $arr);
		
		if ($result != null)
		{
			return $result;
		}	
		
	}
	
	// Obtains all tickets for customer id + all the tickets that can be purchased (thus which customer does not own yet). 
	function ObtainTicketsForPurchase($customerId)
	{
		$sql = 'SELECT e.idEvent, e.Name, DATE(e.Start_Date), DATE(e.End_Date), e.Price, t.idTicket, c.idCampingspot, c.Name FROM event e JOIN ticket t ON (e.idEvent = t.idEvent) LEFT JOIN campingspot c ON (t.idCampingspot = c.idCampingspot) WHERE t.idCustomer = :customerId AND e.IsActivated = 1 UNION SELECT e.idEvent, e.Name, DATE(e.Start_Date), DATE(e.End_Date), e.Price, NULL, NULL, NULL FROM event e WHERE e.idEvent NOT IN (SELECT t.idEvent FROM ticket t WHERE t.idCustomer = :customerId) AND e.IsActivated = 1;';
		$arr = [':customerId'=>$customerId];
		
		$result = querydatabase($sql, $arr);
		
		return $result;
	}
	
	function ObtainComments($eventId, $offset)
	{
		$sql = "SELECT idComment, date, time, Name, content FROM comment_readable_view WHERE idEvent = :eventId ORDER BY idComment DESC LIMIT 25 OFFSET $offset;";
		$arr = [':eventId'=>$eventId];
		
		$result = querydatabase($sql, $arr);
		
		return $result;

	}
	
	// Insert long ass customer information into the customer table.
	function insertCustomerData($fName, $lName, $email, $phone, $street, $houseNr, $pCode, $city, $country, $dob)
	{
		// First try to update (in case the customer is a friend of some1.)
		// Otherwise the data will not be inserted as the email already exists.
		$sql = 'UPDATE customer
				SET	Telephone_Number = :phone, 
					Street_Name = :street, 
					House_Number = :houseNr, 
					Postal_Code = :pCode, 
					City = :city, 
					Country = :country, 
					Dob = :dob
				WHERE Email = :email;';
				
		$arr = [':phone'=>$phone, ':street'=>$street, ':houseNr'=>$houseNr, ':pCode'=>$pCode, ':city'=>$city, ':country'=>$country, ':dob'=>$dob, ':email'=>$email,];
		querydatabase($sql, $arr);
		
		$arr = [':fName'=>$fName, ':lName'=>$lName, ':email'=>$email, ':phone'=>$phone, ':street'=>$street, ':houseNr'=>$houseNr, ':pCode'=>$pCode, ':city'=>$city, ':country'=>$country, ':dob'=>$dob];
		$sql = 'INSERT INTO customer (First_Name, Last_Name, Email, Telephone_Number, Street_Name, House_Number, Postal_Code, City, Country, Dob) VALUES (:fName, :lName, :email, :phone, :street, :houseNr, :pCode, :city, :country, :dob);';
		querydatabase($sql, $arr);
	}
	
	// Function to delete old customer data if the input email already exists.
	function deleteOldCustomerData($email)
	{
		$sql = 'DELETE FROM customer WHERE Email = :email;';
		$arr = [':email'=>$email];
		
		querydatabase($sql, $arr);
	}

	function getAllCampingSpots()
	{
		$sql = "SELECT c.idCampingspot, c.Name, (6 - COUNT(*)) AS available_spots
					FROM campingspot c
					JOIN ticket t
					ON (c.idCampingspot = t.idCampingspot)
					WHERE t.idEvent = 1
					GROUP BY c.Name
					HAVING available_spots > 0
					UNION
					SELECT c.idCampingspot, c.Name, 6
					FROM campingspot c
					WHERE c.idCampingspot NOT IN (
						SELECT t.idCampingspot
						from ticket t
						WHERE t.idEvent = 1
						AND t.idCampingspot IS NOT NULL
					);";

		//$arr = [':idEvent'=>$eventId];
		$arr = [1];

		$result = querydatabase($sql, $arr);

		return $result;

	}

	function insertCampingSpot($email,$campingspot,$idEvent)
	{
		$sql = 'update ticket set idCampingspot = :campingspot where idCustomer = (SELECT idCustomer from customer where email = :email) and idEvent = :idEvent';
		$array = [':campingspot'=>$campingspot,':email'=>$email,':idEvent'=>$idEvent];
		
		querydatabase($sql,$array);
	}

?>