<?php
	session_start();
	include('databasefunctions.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Account</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/eventaccount.css">
	<link rel="stylesheet" href="accountOverviewStyle.css">
	<link rel="stylesheet" href="editDetails.css">
</head>
<body>
<div class="wrapper">
    <div class="panel">
        <div class="panel-header">
            <h3 class="title">
				<?php echo 'Edit Account Details of Customer ID: '. $_SESSION['customerInfo']['idCustomer']; ?>
			</h3>
            <div class="option-views">
				<a href="accountOverview.php"><span>Account overview</span></a>
                <a href="editpersonaldetails.php"><span>Edit personal details</span></a>
            </div>
        </div>
			<div class="panel-body">
				<div id="personDetails">
					<div class="detailBox">
						<div class="detailLabel">
							<label for="fName">First Name</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="fName">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="lName">Last Name</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="lName">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="email">Email</label>
						</div>
							
						<div class="detailInput">
							<input type="text" name="email">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="phone">Phone</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="phone">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="street">Street</label>
						</div>
							
						<div class="detailInput">
							<input type="text" name="street">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="houseNr">House Number</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="houseNr">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="postalCode">Postal Code</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="postalCode">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="City">City</label>
						</div>
						
						<div class="detailInput">
							<input type="text" name="City">
						</div>
					</div>
					
					<div class="detailBox">
						<div class="detailLabel">
							<label for="country">Country</label>
						</div>
						
						<div class="detailLabel">
							<?php require('countries.html'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>