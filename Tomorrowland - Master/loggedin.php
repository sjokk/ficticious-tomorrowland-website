<?php
	session_start();
	include('databasefunctions.php');
	
	// Any thing else redirects to the login page.
	$page = "login";
	
	// Only check the query string for logged in users.
	if ($_SESSION['isLegit'])
	{	// If page was set in the query string $page will be overwritten.
		if ($_GET)
		{
			if (isset($_GET['page']))
			{
				// The page is set by the user.
				$page = $_GET['page'];
			}
		}
	}
	
	// Check on query string.
	switch($page)
	{
		// In case of login, redirect to the login page.
		case "login":
		header('Location: login.php');
		break;
		
		// In case of all the other legit pages we should check if the user is logged in.
		case "accountOverview":
		
		// Obtain user's information.
		$ticketRows = ObtainTicketsForPurchase($_SESSION['customerInfo']['idCustomer']);
		
		// Set title
		$pageTitle = "Account Overview";
		$prefix = "";
		if (isset($_GET['edit']))
		{
			$pageTitle = 'Edit Account Overview';
			$prefix = 'Edit ';
		}
		$header3 = $prefix .'Account overview of Customer id: '. $_SESSION['customerInfo']['idCustomer'];
		
		// Set account overview as body for panel.
		$body = 'accountOverviewBody.html';
		
		// Include panel with body.
		include('panel.html');
		break;
		// ----- END OF ACCOUNT OVERVIEW -----
		
		
		// In case of all the other legit pages we should check if the user is logged in.
		case "ticketId":
		
		// Obtain user's information.
		$ticketRows = ObtainTicketsForPurchase($_SESSION['customerInfo']['idCustomer']);
		
		// Set title
		$pageTitle = "Event Account";
		$header3 = 'My event account id: '. $_GET['value'];
		
		$flag = false;
		$counter = 0;

		// Exhaust the array until we reach the end or find that
		// the user querying for a page does indeed hold that ticketId.
		while (($flag == false) && ($counter < count($ticketRows)))
		{	
			if ($ticketRows[$counter]['idTicket'] == $_GET['value'])
			{
				$flag = true;
			}
			$counter++;
		}
		
		if (!$flag) // Person did not hold entered ticket id.
		{
			header('Location: loggedin.php?page=AccountOverview');
		}
		else // s/he (insert favorite pronoun does.
		{
			// Set account overview as body for panel.
			$body = 'myeventaccount.html';
			$action = '';
			$payment = 'amount.html'; // HTML that will be included in the form of the modal box (Credit card html thing).
			// Include panel with body.
			include('panel2.html');
		}
		break;
		// ----- END OF EVENT ACCOUNT OF TICKET ID -----
		
		case "purchaseticket":
		$pageTitle = "Purchase Ticket";
		
		if (isset($_GET['value']))
		{
			$eventId = $_GET['value'];
			$eventInfo = ObtainEvent($eventId);
		}
		
		// If the get value (eventId) doesnt return an activated event,
		// the customer is redirected to his/her account overview.
		if (!isset($eventInfo) || $eventInfo == null)
		{
			header('Location: loggedin.php?page=AccountOverview');
		}
		// Otherwise we include the body that allows ticket purchase.
		else
		{
			if (!isset($_POST) || $_POST == null)
			{
				$header3 = 'Purchase Ticket for '. $eventInfo['Name'];
				$body = 'ticketPurchase.html'; // 
				//$payment = 'empty.html'; // HTML that will be included in the form of the modal box (Credit card html thing).
				include('panel.html');
			}
			else 
			{
				// Assign values for usage throughout the purchase.
				$_SESSION['purchase'][$eventId]['numberOfTickets'] = $_POST['numberOfTickets'];
				if (isset($_POST['campingspot']))
				{
					$_SESSION['purchase'][$eventId]['campingSpot'] = $_POST['campingspot'];
				}
				else
				{
					$_SESSION['purchase'][$eventId]['campingSpot'] = 0;
				}
				
				// Post is set and we want the user to purchase tickets.
				$header3 = 'Purchase Ticket for '. $eventInfo['Name'];
				
				// Insert the purchase of tickets body. 
				$body = 'purchase.html';
				require('panel.html');
			}
		}
		break;		
		// ----- END OF PURCHASE TICKET -----

		// ----- END OF FINALIZING PURCHASING TICKET -----
        case "reservecampingspot":
		$ticketRows = ObtainTicketsForPurchase($_SESSION['customerInfo']['idCustomer']);
		$eventId = ObtainTicketRow($_GET['value'])['idEvent'];
		$campingRows = ObtainAvailableCampingSpots($eventId);
		$pageTitle = "Camping spot reservation";
		$header3 =  'Reserve camping spot for: ' .$_GET['value'];

		$flag = false;
		$counter = 0;

		while (($flag == false) && ($counter < count($ticketRows)))
		{
			if ($ticketRows[$counter]['idTicket'] == $_GET['value'])
			{
				$flag = true;
			}
			$counter++;
		}

		if (!$flag) // Person did not hold entered ticket id.
		{
			header('Location: loggedin.php?page=AccountOverview');
		}
		else
		{
			$body = 'campingres.php';
			$payment = 'amount.html';
			include('panel.html');
		}
		break;

		default:
		header('Location: loggedin.php?page=accountOverview');
		break;
	}
?>