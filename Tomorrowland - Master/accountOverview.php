<?php
	session_start();
	include('databasefunctions.php');
	
	// Obtain stored e-mail from started session.
	$email = $_SESSION['email'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Account Overview</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/eventaccount.css">
	<link rel="stylesheet" href="accountOverviewStyle.css">
	
</head>
<body>
<div class="wrapper">
    <div class="panel">
        <div class="panel-header">
            <h3 class="title">
				<?php echo 'Account overview of Customer id: '. $_SESSION['customerInfo']['idCustomer']; ?>
			</h3>

            <div class="option-views">
                <a href="#"><span>
					Account Overview
				</span></a>
				<!-- This only available from of the log in landing page. -->
				<!-- <a href="myeventaccount.php"><span>Event account</span></a> -->
				<!-- Only viewable from of the event account page (maybe use
				the pop up button that is already there? -->
                <!-- <a href="deposit.php"><span>Transaction history</span></a> -->
                <!-- <a href="editpersonaldetails.php"><span>Edit personal details</span></a> -->
            </div>
        </div>
		<div class="panel-body">
            <div class="accountoverview">
				<!-- Customer information table. -->
				<?php
					if ($_GET)
					{
						if (isset($_GET['edit']))
						{
							echo '<form id="editDetails" action="submitChanges.php">';
						}
					}
					else
					{
						echo '<form>';
					}
				?>
				<div id="tableBox">
					<table id="customerInformationTable">
						<?php
							if ($_GET)
							{
								if (isset($_GET['edit']))
								{
									require('editDetails.html');
								}
							}
							else
							{
								include('personalDetails.html');
							}
						?>
					</table>
				</div>
				<?php
				if ($_GET)
				{
					// If edit is pressed.
					if (isset($_GET['edit']))
					{
						echo '<div id="editBtnBox">';
						echo '<input id="saveBtn" class="btn btn-primary-derived" type="submit" form="editDetails" name="save" value="Save">';
						echo '<input id="cancelBtn" class="btn btn-primary-derived" type="button" id="cancelBtn" onclick="overview()" value="Cancel">';
						echo '</div>';
					}
				}
				else
				{	
					echo '<div id="editBbtBox">';
					echo '<input id="editBtn" class="btn btn-primary-derived" type="submit" name="edit" value="Edit">';
					echo '</div>';
				}
				?>
				</form>
            </div>
			<!-- To be filled with tickets owned by customer and available for purchase. -->
			<div id="ticketBox">
				<table class="accountOverviewTables table-responsive">
					<tbody>
						<tr>
							<th>Event Name</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Price</th>
							<th>Ticket ID</th>
							<th>Action</th>
							<th>Camping Spot</th>
						</tr>
					<?php
						$counter = 0;
						foreach ($ticketRows as $ticketRow)
						{
							$counter += 1;
							
							echo '<tr>';
								// print every element in row into a td element.
								echo '<td class="eventTds">'. $ticketRow['Name'] .'</td>';
								echo '<td class="eventTds">'. $ticketRow['Start_Date'] .'</td>';
								echo '<td class="eventTds">'. $ticketRow['End_Date'] .'</td>';
								echo '<td class="eventTds"> €'. $ticketRow['Price'] .'</td>';
								
								// If there exists a value for ticket id.
								if (isset($ticketRow['idTicket']))
								{
									$aText = 'Event Account';
									$script = "myeventaccount.php?ticketId=". $ticketRow['idTicket'];
									echo '<td class="eventTds">'. $ticketRow['idTicket'] .'</td>';
									
									// If there exists a value for camping spot id, display the camping's name in the table.
									if (isset($ticketRow['idCampingspot']))
									{
										$campingSpot = $ticketRow['CampingSpotName'];
									}
									else // If there does not exits one, create an anchor tag that redirects to a new php page where the customer can reserve a camping spot for that ticket.
									{
										$campingSpot = '<a href="reserveCampingspot.php?ticketId='. $ticketRow['idTicket'] .'">Reserve Campingspot</a>';
									}
								}
								else
								{ // If there does not exists a value for ticket id, adjust the table data's text to Purchase and provide a purchase option for that event.
									$aText = 'Purchase';
									$script = "purchaseTicket.php?eventId=". $ticketRow['idEvent'];
									echo '<td class="eventTds">None</td>';
									$campingSpot = 'Not Available';
								}
								echo '<td class="eventTds"><a class="btn btn-primary btn-xs" href="'. $script .'">'. $aText .'</a></td>';
								echo '<td>'. $campingSpot .'</td>';
								
							echo '</tr>';
							
							if ($counter == 8) // Limit the number of rows to equal that over the person's information table just to make sure we don't exceed any visuals.
							{
								break;
							}
						}
					?>
					</tbody>
				</table>
			</div>
        </div>
    </div>
	<script>
		// Redirect back to overview on pressing cancel.
		function overview() {
			window.location.href = "accountOverview.php";
		}
	</script>
</body>
</html>