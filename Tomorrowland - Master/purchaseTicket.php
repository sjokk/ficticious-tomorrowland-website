<?php
	function sendPurchaseConfirmation($email, $ticketId)
	{	
		$customerInformation = ObtainCustomerInformation($email);
		
		$fName = $customerInformation['First_Name'];

		$subject = "Ticket Purchase";
		
		// Change the url in the e-mail to the url of the host of the website.
		$message = "
		<html>
			<body>
				<div style='height: 400px; overflow: hidden;'>
					<img style='width: 100%;' src='https://i231896.hera.fhict.nl/Tomorrowland/assets/images/backgrounds/Partypeople.jpg'>
				</div>
				
				<div style='margin-top: 35px; padding-left: 20px; max-width: 600px;'>
					
					<h3>Dear $fName</h3>
					
					<p>Thanks for purchasing a TomorrowLand ticket!</p>
					
					<p>Below we summarized a few details that are of your interest.</p>
					
					<p><b>IMPORTANT:</b><br>
						Once the event starts you can use your ticket QR-code
						at the event entrance and you will receive a bracelet
						with which you can enter and leave the event as well as 
						the camping area (if you reserved a camping spot).
					</p>
					<p>
						The bracelet can then also be used at the event to 
						up your account balance and make payments to buy food
						and drinks.
					</p>
					<p>				
						We hope you will have a wonderful time at the event!
					</p>
			
					<b>YOUR QR-CODE:</b>
					<em>Do not share this with anyone else but the employee at the event entrance</em>
					
					<br>
					<br>
					
					<div id='ticketQr'>
						<img style='width: 200px;' src='https://barcode.tec-it.com/barcode.ashx?data=$ticketId&code=QRCode&dpi=96&dataseparator=' alt='Barcode Generator TEC-IT'/>
					</div>
					
					<br>
					
					<p>You can always obtain your ticket QR-Code on your event account page on the website.</p>
					
					<p>With kind regards,</p>
			
					<p>The TomorrowLand Team!</p>
					
				</div>
			</body>
		</html>
		";
		
		$headers = "From:noreply@TomorrowLand.com" . "\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($email, $subject, $message, $headers);
	}
	
	function sendFriendPurchaseConfirmation($email, $ticketId, $purchaser)
	{	
		$customerInformation = ObtainCustomerInformation($email);
		
		$fName = $customerInformation['First_Name'];

		$subject = "Friend Ticket Purchase";
		
		// Change the url in the e-mail to the url of the host of the website.
		$message = "
		<html>
			<body>
				<div style='height: 400px; overflow: hidden;'>
					<img style='width: 100%;' src='https://i231896.hera.fhict.nl/Tomorrowland/assets/images/backgrounds/Partypeople.jpg'>
				</div>
				
				<div style='margin-top: 35px; padding-left: 20px; max-width: 600px;'>
					
					<h3>Dear $fName</h3>
					
					<p>Your friend $purchaser purchased a TomorrowLand ticket for you!</p>
					
					<p>Below we summarized a few details that are of your interest.</p>
					
					
					<p><b>IMPORTANT:</b><br>
						In case you do not have an account yet, we recommend you do this
						online at: <b><a href='https://i231896.hera.fhict.nl/Tomorrowland/Signup.php'>Sign up</a></b>
						<br>
						Once you created an account you can find your details there, such as tickets,
						camping spot reservations, and much more.
						
						<br>
						
						or log in at: <a href='https://i231896.hera.fhict.nl/Tomorrowland/login.php'>Log in</a> to view your tickets.
					</p>
					<p>
						You are not required to create an account beforehand, but this will save you
						a significant amount of time as you will have to do so at the event entrance
						otherwise.
					</p>
					<p>
						Once the event starts you can use your ticket QR-code
						at the event entrance and you will receive a bracelet
						with which you can enter and leave the event as well as 
						the camping area (if you reserved a camping spot).
					</p>
					<p>
						The bracelet can then also be used at the event to 
						up your account balance and make payments to buy food
						and drinks.
					</p>
					<p>				
						We hope you will have a wonderful time at the event!
					</p>
			
					<b>YOUR QR-CODE:</b>
					<em>Do not share this with anyone else but the employee at the event entrance</em>
					
					<br>
					<br>
					
					<div id='ticketQr'>
						<img style='width: 200px;' src='https://barcode.tec-it.com/barcode.ashx?data=$ticketId&code=QRCode&dpi=96&dataseparator=' alt='Barcode Generator TEC-IT'/>
					</div>
					
					<br>
					
					<p>You can always obtain your ticket QR-Code on your event account page on the website.</p>
					
					<p>With kind regards,</p>
			
					<p>The TomorrowLand Team!</p>
					
				</div>
			</body>
		</html>
		";
		
		$headers = "From:noreply@TomorrowLand.com" . "\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($email, $subject, $message, $headers);
	}
?>