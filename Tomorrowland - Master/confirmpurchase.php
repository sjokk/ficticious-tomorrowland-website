<?php
	session_start();
	
	if (isset($_POST) && $_POST != null)
	{
		$eventId = $_POST['eventId'];
		// The person is logged in and came from a purchase page.
		if (isset($_SESSION['purchase'][$eventId]) && $_SESSION['purchase'][$eventId] != null)
		{			
			//print_r($_SESSION);
			//print_r($_POST);
			
			// purchase a ticket for self and send confirmation email.
			include('databasefunctions.php');
			include('purchaseTicket.php');
			$purchaser_ticketId = PurchaseTicket($_SESSION['customerInfo']['idCustomer'], $eventId)[0];
			// sendPurchaseConfirmation($_SESSION['customerInfo']['Email'], $purchaser_ticketId);
			
			$purchaser = $_SESSION['customerInfo']['First_Name'];
			$purchaser .= ' ';
			$purchaser .= $_SESSION['customerInfo']['Last_Name'];
			
			// For friends of purchaser.
			if ($_SESSION['purchase'][$eventId]['numberOfTickets'] > 1)
			{
				$_SESSION['purchase']['friends'] = $_POST;
				
				for ($i = 1; $i <= ($_SESSION['purchase'][$eventId]['numberOfTickets'] - 1); $i++)
				{
					// Create a customer for every inserted fname, lname and email.
					$fname = 'fName'. $i;
					$lname = 'lName'. $i;
					$email = 'email'. $i;
					
					$customerId = CreateCustomerIfNotExists($_POST[$fname], $_POST[$lname], $_POST[$email])[0];
					
					// immediately purchase a ticket with that customer id.
					$ticketId = PurchaseTicket($customerId, $eventId)[0];
					
					// send ticket purchase email.
					// sendFriendPurchaseConfirmation($_POST[$email], $ticketId, $purchaser);
				}
			}
			// Empty all fields but the once relevant for reserving a camping spot.
			
			// If the user flagged for reserving a campingspot, direct him/her towards that
			if ($_SESSION['purchase'][$eventId]['campingSpot'] == 1)
			{
				// Redirect to the camping page.
				$url = 'loggedin.php?page=reservecampingspot&value=';
				$url .= $purchaser_ticketId;
				// echo $url;
				header('Location: '. $url);
			}
			else
			{
				// Else finalize purchase, show a confirmation of the tickets purchased on a new page.
				$pageTitle = 'Confirmation';
				$header3 = 'Purchase Confirmation';
				$body = 'ticketPurchaseConfirmation.html';
				
				include('panel.html');
			}
		}
		else
		{
			echo '<div>Something went wrong.</div>';
			echo '<div><p>We are sorry for the inconvenience.</p></div>';
			echo '<a href="loggedin.php?page=AccountOverview">Back to Account Overview</a>';
		}
	}
	else
	{
		// Redirect to log in page.
		header('Location: loggedin.php?page=AccountOverview');
	}
?>