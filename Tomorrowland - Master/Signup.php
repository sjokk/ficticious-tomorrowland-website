<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title>Sign up</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- css -->
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="loginstyle.css">
    
</head>
<body>
    <nav id="site-nav" class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="./">
                        <!-- logo image  -->
                        <img src="assets/images/logo.png" alt="Logo">   
                    </a>
                </div>
            </div><!-- /.navbar-header -->

            <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <li class="active"><a href="./">Back</a></li>
                    <li><a href="experience.php">Experience</a></li>
                    <!-- <li> <a href ="login.php" ><i><u>Log in</u></i></a></li> -->
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>

	<div class="log-form">

	    <h2>Account details</h2>

		<form action="signupfunc.php" method="post">

			<input type="email" name="email" id="email" placeholder="user@example.com" maxlength="50" required/>
			<input type="password" name="password" id="password" placeholder="Password" required/>
			<input type="password" name="confirmpassword" id="confirmpassword" placeholder="Confirm password" required/>
            <a class="forgot" href="login.php">Already have an account?<b>Log In!</b></a>
	</div>

  <!--end log form1 -->
    <div class="log-form2">

	    <h2>Your details</h2>

		    <input type="text" name="firstname" id="firstname "placeholder="First name" maxlength="50" required />
		    <input type="text" name="lastname" placeholder="Last name" required/>
		    <input type="text" name="address" placeholder="Address" required/>
		    <input type="text" name="number"  placeholder="Street number" required />
		    <input type="text" name="postcode" placeholder="Postal code" required/>
		    <input type="text" name="city" placeholder="City" required/>

			<div class="country">
				<?php require('countries.html'); ?>
			</div>
		
		    <input type="text" name="telephonenumber" placeholder="Mobile number" required/>
		    <input type="date" name="dob" placeholder="Date of birth" required/>
			
	    <div class="submitbutton">
		    <button type="submit" class="btn">Submit</button>
	    </div>
		
        </form>
    </div>
</body>
</html>