<!DOCTYPE html>

<?php include('campingfunc.php');?>
<html lang="en"></html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Account Overview</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/eventaccount.css">
	<link rel="stylesheet" href="accountOverviewStyle.css">
	
	
</head>
<body>
<div id="campmap">
			
			<a href="assets/images/backgrounds/Map.jpg" target="<img src="assets/images/backgrounds/Map.jpg">Click to view the camping grounds</a>
			
			
			<br><button type="button" class="btn btn-primary submitbutton" id="myBtn" name="submit">Reserve Camping Spot(s)</button></br>
			
			<?php require('ModalBoxCamping.html'); ?>
		
</div>
	
<div id="campingBox">
				<table class="campingOverviewTables table-responsive">
					<tbody>
						<tr>
							<th>Camping spot name</th>
							<th>Location</th>
							<th>Spots available</th>
							
							
						</tr>
					<?php
						$counter = 0;
						foreach ($campingRows as $row)
						{
							
							
							
							
							if($counter < 10)
							{
								$counter += 1;
							echo '<tr>';
								// print every element in row into a td element.
								echo '<td>'. $row['Name'] .'</td>';
								echo '<td>'. $row['idCampingspot'] .'</td>';
								echo '<td>'. $row['available_spots'] .'</td>';
								
								
							
							echo '</tr>';
							}
							
							
							
						}
					?>
					</tbody>
				</table>
</div>
		<table class="campingOverviewTables2 table-responsive">
					<tbody>
					<tr>
							<th>Camping spot name</th>
							<th>Location</th>
							<th>Spots available</th>
							
							
					</tr>
					<?php
						$counter2 = 0;
						foreach ($campingRows as $row)
						{
							$counter2++;
							
							if($counter2 > 10)
							{
								echo '<tr>';
								// print every element in row into a td element.
								echo '<td>'. $row['Name'] .'</td>';
								echo '<td>'. $row['idCampingspot'] .'</td>';
								echo '<td>'. $row['available_spots'] .'</td>';
								
								echo '</tr>';
							}
						}
					?>
			
					</tbody>
				
				</table>
</body>