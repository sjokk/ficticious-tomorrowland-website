<?php 
	session_start();
	
	if (isset($_SESSION['isLegit']))
	{
		if (isset($_SESSION['isLegit']))
		{
			header('Location: loggedin.php?page=accountOverview');
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Log in</title>

	<!-- Latest compiled and minified CSS // TO BE REVIEWED -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="loginstyle.css">
	

	<!-- jQuery library // TO BE REVIEWED -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled JavaScript TO BE REVIEWED -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 	
</head>
<body>
        
	<nav id="site-nav" class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="./">
                        <!-- logo image  -->
                        <img src="assets/images/logo.png" alt="Logo">
                    </a>
                </div>
            </div><!-- /.navbar-header -->

            <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">
                    <!-- navigation menu -->
                    <li class="active"><a href="./">Back</a></li>
                    <li><a href="experience.php">Experience</a></li>
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>
        
	<div class="log-form">

		<h2>Login to your event account</h2>
		<form action="loginfunc.php" method="post">

			<input type ="email" name="email" id="email" placeholder="user@example.com" maxlength="50" required/>
			<input type="password" name="password" placeholder="password" maxlength="300" required/>
			<div>
			<div>
			<button type="submit" class="btn">Login</button>
			</div>
			<div>
			<a class="forgot" href="Signup.php">Don't have an account? Sign up!</a>
			<a class="forgot" data-toggle="" data-target="" style="clear: both;" href="#">Forgot your password?</a>
			<a class="forgot" style="clear: both;" href="#">Didn't receive your activation mail?</a>
			</div>
		</div>
		</form>
	</div><!--end log form -->
</body>
</html>