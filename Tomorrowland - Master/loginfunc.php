<?php
	include('databasefunctions.php');

	$email = $_POST['email'];
	$password = $_POST['password'];

	$result = CheckLogIn($email);

	if($result)
	{
		//check if the login exists or not
		//retrieve the hashed password from the db	
		$userInformation = ObtainUserInformation($email);
		
		if($userInformation['isConfirmed'] == 1)
		{
			$hashedpassword = $userInformation['password'];
			if(password_verify($password, $hashedpassword))
			{
				session_start();
				
				$_SESSION['customerInfo'] = ObtainCustomerInformation($email);
				$_SESSION['email'] = $email;
				$_SESSION['isLegit'] = true;


				echo '<div class=logo>';
				echo '<center><img src="assets/images/backgrounds/sticker-logo-tomorrowland-6598.png"/></center>';
				echo '</div>';
				echo "<center><div class=\"loader\"></div></center>";
				echo"</html>";

				header("refresh:3; url=loggedin.php?page=accountOverview");
			}
			else
			{
				echo "<h1>incorrect email or password</h1>";
				header ("refresh:3; url=login.php");
			}
		}
		else
		{
			echo "Your email is not yet verified, can you please click the acivation link you received on email";
		}
	}
	else
	{
		echo "Incorrect credentials";
	}
?>
<style>
	<?php include('loader.css'); ?>
</style>
