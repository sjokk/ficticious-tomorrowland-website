<?php
	session_start();
	include('databasefunctions.php');

	$ticketId = $_GET['ticketId'];
	$compEmail = ObtainCustomerEmailForTicketId($ticketId);
	
	if (($compEmail['Email'] != null) && ($compEmail['Email'] == $_SESSION['customerInfo']['Email']))
	{	
		// POTENTIALLY HAVE TO INSERT A CHECK (ISVALID) TICKET ATTRIBUTE TO SEE IF
		// THE EVENT MANAGEMENT STILL ALLOWS THIS EVENT ACCOUNT TO BE USED.
		if ((isset($_POST['amount'])) && ($_POST['amount'] != null))
		{		
			// Update balance for this ticket id.
			updateBalance($ticketId, $_POST['amount']);
			
		}
		// Obtain balance for this ticketId.
		$allTicketInfo = ObtainTicketInformation($_SESSION['customerInfo']['idCustomer'], $ticketId);
		
		header('url=myeventaccount.php')
	}
	else
	{
		header('url=/');
	}
?>