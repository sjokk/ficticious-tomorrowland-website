<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>$pageTitle</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/eventaccount.css">
	<link rel="stylesheet" href="accountOverviewStyle.css">
	
</head>
<body>
<div class="wrapper">
    <div class="panel">
        <div class="panel-header">
            <h3 class="title">
				<!-- Create H3 title based on case in switch. -->
				<?php echo 'Account overview of Customer id: '. $_SESSION['customerInfo']['idCustomer']; ?>
			</h3>

            <div class="option-views">
                <a href="#"><span>
					Account Overview
				</span></a>
            </div>
        </div>
		<div class="panel-body">

			<!-- Include the content of account overview -->
			include($body);
		
        </div>
    </div>
</body>
</html>